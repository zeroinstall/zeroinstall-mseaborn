#!/usr/bin/env python2.4
import sys, tempfile, os, StringIO
import unittest
import logging
import warnings

# It's OK to test deprecated features
warnings.filterwarnings("ignore", category = DeprecationWarning)

# Catch silly mistakes...
os.environ['HOME'] = '/home/idontexist'

sys.path.insert(0, '..')
from zeroinstall.injector import namespaces, qdom
from zeroinstall.injector import iface_cache, download, distro
from zeroinstall.zerostore import Store; Store._add_with_helper = lambda *unused: False
from zeroinstall import support
from zeroinstall.support import basedir

dpkgdir = os.path.join(os.path.dirname(__file__), 'dpkg')

empty_feed = qdom.parse(StringIO.StringIO("""<interface xmlns='http://zero-install.sourceforge.net/2004/injector/interface'>
<name>Empty</name>
<summary>just for testing</summary>
</interface>"""))


def write_file(filename, data):
	fh = open(filename, "w")
	try:
		fh.write(data)
	finally:
		fh.close()


def assert_sets_equal(actual, expected):
	if sorted(actual) != sorted(expected):
		missing = [x for x in expected if x not in actual]
		excess = [x for x in actual if x not in expected]
		raise AssertionError(
			"Sets do not match:\nmissing: %r\nexcess: %r" %
			(missing, excess))


class BaseTest(unittest.TestCase):
	def setUp(self):
		self._on_teardown = []
		self.config_home = self.make_temp_dir()
		self.cache_home = self.make_temp_dir()
		self.cache_system = self.make_temp_dir()
		self.gnupg_home = self.make_temp_dir()
		os.chmod(self.cache_system, 0500) # Read-only
		os.environ['GNUPGHOME'] = self.gnupg_home
		os.environ['XDG_CONFIG_HOME'] = self.config_home
		os.environ['XDG_CACHE_HOME'] = self.cache_home
		os.environ['XDG_CACHE_DIRS'] = self.cache_system
		reload(basedir)
		assert basedir.xdg_config_home == self.config_home
		iface_cache.iface_cache.__init__()

		if os.environ.has_key('DISPLAY'):
			del os.environ['DISPLAY']
		namespaces.injector_gui_uri = os.path.join(os.path.dirname(__file__), 'test-gui.xml')

		logging.getLogger().setLevel(logging.WARN)

		download._downloads = {}

		self.old_path = os.environ['PATH']
		os.environ['PATH'] = dpkgdir + ':' + self.old_path

		distro._host_distribution = distro.DebianDistribution(dpkgdir)
		self._old_environ = os.environ.copy()
		self._changes_environ = False
	
	def make_temp_dir(self):
		temp_dir = tempfile.mkdtemp(
			prefix="tmp-%s-" % self.__class__.__name__)
		def tear_down():
			support.ro_rmtree(temp_dir)
		self._on_teardown.append(tear_down)
		return temp_dir

	def make_temp_file(self, data):
		filename = os.path.join(self.make_temp_dir(), "file")
		write_file(filename, data)
		return filename

	def patch_env_var(self, key, value):
		old_value = os.environ.get(key)
		def restore():
			if old_value is None:
				if key in os.environ:
					del os.environ[key]
			else:
				os.environ[key] = old_value
		self._on_teardown.append(restore)
		os.environ[key] = value

	def changes_environ(self):
		self._changes_environ = True

	def tearDown(self):
		for func in reversed(self._on_teardown):
			func()
		if self._changes_environ:
			self.assertNotEqual(os.environ, self._old_environ)
		else:
			assert_sets_equal(set(os.environ.iteritems()),
					  set(self._old_environ.iteritems()))
		os.environ['PATH'] = self.old_path
