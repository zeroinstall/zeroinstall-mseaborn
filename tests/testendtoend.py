#!/usr/bin/env python2.4
import StringIO
import os
import subprocess
import sys
import unittest

sys.path.insert(0, '..')

from basetest import BaseTest
from basetest import write_file
from zeroinstall.injector import autopolicy
from zeroinstall.injector import qdom
from zeroinstall.injector import run
from zeroinstall.injector import selections
from zeroinstall.injector.iface_cache import iface_cache
from zeroinstall.zerostore import manifest


# Based on code in 0publish/archive.py
def manifest_for_dir(dir):
	algorithm = manifest.get_algorithm("sha1new")
        digest = algorithm.new_digest()
        for line in algorithm.generate_manifest(dir):
                digest.update(line + '\n')
	return algorithm.getID(digest)


def selections_for_feed(feed_file):
	policy = autopolicy.AutoPolicy(
		feed_file, download_only=True, dry_run=False)
	policy.download_and_execute([])
	return selections.Selections(policy)


class FileTreeWriter(object):

	def __init__(self, files):
		self._files = files

	def write_tree(self, dest_path):
		for filename, data in self._files:
			pathname = os.path.join(dest_path, filename)
			if not os.path.exists(os.path.dirname(pathname)):
				os.makedirs(os.path.dirname(pathname))
			write_file(pathname, data)
			# Making everything executable is simpler.
			os.chmod(pathname, 0777)


class TestEndToEnd(BaseTest):
	def _make_archive(self, tree):
		tar_file = os.path.join(self.make_temp_dir(), "archive.tar.gz")
		temp_dir = self.make_temp_dir()
		tree.write_tree(temp_dir)
		subprocess.check_call(["tar", "-czf", tar_file, "-C", temp_dir, "."])
		return """
      <implementation id="%s" version="1">
        <archive href="%s" size="%i"/>
      </implementation>
      """ % (manifest_for_dir(temp_dir), tar_file, os.path.getsize(tar_file))

	def _check_output(self, selns, expected_output):
		env = {}
		run.add_env_bindings(env, selns)
		cmd_path = run.get_executable_for_selections(selns)
		proc = subprocess.Popen([cmd_path], env=env, stdout=subprocess.PIPE)
		stdout, stderr = proc.communicate()
		self.assertEquals(proc.wait(), 0)
		self.assertEquals(stdout, expected_output)

		# Check that it also works with the selections are
		# saved to XML.
		selections_xml = selns.toDOM().toxml()
		selections_qdom = qdom.parse(StringIO.StringIO(str(selections_xml)))
		selns2 = selections.Selections(selections_qdom)
		self.assertEquals(selections_xml, selns2.toDOM().toxml())
		env = {}
		run.add_env_bindings(env, selns2)
		cmd_path = run.get_executable_for_selections(selns2)
		proc = subprocess.Popen([cmd_path], env=env, stdout=subprocess.PIPE)
		stdout, stderr = proc.communicate()
		self.assertEquals(proc.wait(), 0)
		self.assertEquals(stdout, expected_output)

	def test_module_with_no_dependencies(self):
		tree = FileTreeWriter([("hello-test", """\
#!/bin/sh
echo Hello world!
""")])
		feed_file = self.make_temp_file("""
<interface xmlns="http://zero-install.sourceforge.net/2004/injector/interface">
  <name>Test</name>
  <summary>Placeholder</summary>
  <group main="hello-test">
    %s
  </group>
</interface>
""" % self._make_archive(tree))
		selns = selections_for_feed(feed_file)
		self._check_output(selns, "Hello world!\n")

	def test_module_with_dependency(self):
		library_tree = FileTreeWriter([("share/useful", "useful-contents\n")])
		main_tree = FileTreeWriter([("bin/hello-test", """\
#!/bin/sh
echo Got:
cat $LIBFOO/share/useful
""")])
		library_feed = self.make_temp_file("""\
<?xml version="1.0" ?>
<interface xmlns="http://zero-install.sourceforge.net/2004/injector/interface">
  <name>Test</name>
  <summary>Placeholder</summary>
  <group>
    %s
  </group>
</interface>
""" % self._make_archive(library_tree))
		main_feed = self.make_temp_file("""
<interface xmlns="http://zero-install.sourceforge.net/2004/injector/interface">
  <name>Test</name>
  <summary>Placeholder</summary>
  <group main="bin/hello-test">
    %s
    <requires interface="%s">
      <environment name="LIBFOO" insert="."/>
    </requires>
  </group>
</interface>
""" % (self._make_archive(main_tree), library_feed))
		selns = selections_for_feed(main_feed)
		env = {}
		run.add_env_bindings(env, selns)
		self.assertEquals(env.keys(), ["LIBFOO"])
		self._check_output(selns, "Got:\nuseful-contents\n")

	def test_self_environment_binding(self):
		tree = FileTreeWriter([("share/useful", "useful-contents\n"),
				       ("bin/hello-test", """\
#!/bin/sh
echo Got:
cat $MY_DATA_DIR/share/useful
""")])
		feed_file = self.make_temp_file("""
<interface xmlns="http://zero-install.sourceforge.net/2004/injector/interface">
  <name>Test</name>
  <summary>Placeholder</summary>
  <group main="bin/hello-test">
    %s
    <environment name="MY_DATA_DIR" insert="."/>
  </group>
</interface>
""" % self._make_archive(tree))
		selns = selections_for_feed(feed_file)
		env = {}
		run.add_env_bindings(env, selns)
		self.assertEquals(env.keys(), ["MY_DATA_DIR"])
		self._check_output(selns, "Got:\nuseful-contents\n")

	def test_module_with_substitution_dependency(self):
		library_tree = FileTreeWriter([("share/useful", "useful-contents\n")])
		main_tree = FileTreeWriter([("bin/hello-test", """\
#!/bin/sh
echo Got:
cat MAGIC_EMBEDDED_STRING_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX/share/useful
""")])
		library_feed = self.make_temp_file("""\
<?xml version="1.0" ?>
<interface xmlns="http://zero-install.sourceforge.net/2004/injector/interface">
  <name>Test</name>
  <summary>Placeholder</summary>
  <group>
    %s
  </group>
</interface>
""" % self._make_archive(library_tree))
		main_feed = self.make_temp_file("""
<interface xmlns="http://zero-install.sourceforge.net/2004/injector/interface">
  <name>Test</name>
  <summary>Placeholder</summary>
  <group main="bin/hello-test">
    %s
    <requires interface="%s">
      <rewrite from="MAGIC_EMBEDDED_STRING_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"/>
    </requires>
  </group>
</interface>
""" % (self._make_archive(main_tree), library_feed))
		selns = selections_for_feed(main_feed)
		env = {}
		run.add_env_bindings(env, selns)
		self.assertEquals(env.keys(), [])
		self._check_output(selns, "Got:\nuseful-contents\n")
		# Check that rewritten copies are cached.
		self.assertEquals(run.get_executable_for_selections(selns),
				  run.get_executable_for_selections(selns))

	def test_self_reference_substitution(self):
		tree = FileTreeWriter([("share/useful", "useful-contents\n"),
				       ("bin/hello-test", """\
#!/bin/sh
echo Got:
cat MAGIC_EMBEDDED_STRING_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX/share/useful
""")])
		feed_file = self.make_temp_file("""
<interface xmlns="http://zero-install.sourceforge.net/2004/injector/interface">
  <name>Test</name>
  <summary>Placeholder</summary>
  <group main="bin/hello-test">
    %s
    <rewrite from="MAGIC_EMBEDDED_STRING_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"/>
  </group>
</interface>
""" % self._make_archive(tree))
		selns = selections_for_feed(feed_file)
		env = {}
		run.add_env_bindings(env, selns)
		self.assertEquals(env.keys(), [])
		self._check_output(selns, "Got:\nuseful-contents\n")
		# Check that rewritten copies are cached.
		self.assertEquals(run.get_executable_for_selections(selns),
				  run.get_executable_for_selections(selns))

	def test_fixed_length_substitution_in_dependency(self):
		original = "MAGIC_EMBEDDED_STRING_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
		tree = FileTreeWriter([("bin/hello-test", """\
#!/bin/sh
echo -n MAGIC_EMBEDDED_STRING_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
""")])
		feed_file = self.make_temp_file("""
<interface xmlns="http://zero-install.sourceforge.net/2004/injector/interface">
  <name>Test</name>
  <summary>Placeholder</summary>
  <group main="bin/hello-test">
    %s
    <rewrite from="MAGIC_EMBEDDED_STRING_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"/>
  </group>
</interface>
""" % self._make_archive(tree))
		selns = selections_for_feed(feed_file)
		cmd_path = run.get_executable_for_selections(selns)
		proc = subprocess.Popen([cmd_path], stdout=subprocess.PIPE)
		stdout, stderr = proc.communicate()
		self.assertEquals(proc.wait(), 0)
		self.assertNotEquals(stdout, original)
		self.assertEquals(len(stdout), len(original))

	def test_fixed_length_substitution_in_self_dependency(self):
		original = "MAGIC_EMBEDDED_STRING_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
		library_tree = FileTreeWriter([])
		binary_data = "".join(chr(c) for c in range(256))
		main_tree = FileTreeWriter([("bin/hello-test", """\
#!/bin/sh
echo -n MAGIC_EMBEDDED_STRING_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
exit
""" + binary_data)])
		library_feed = self.make_temp_file("""\
<?xml version="1.0" ?>
<interface xmlns="http://zero-install.sourceforge.net/2004/injector/interface">
  <name>Test</name>
  <summary>Placeholder</summary>
  <group>%s</group>
</interface>
""" % self._make_archive(library_tree))
		main_feed = self.make_temp_file("""
<interface xmlns="http://zero-install.sourceforge.net/2004/injector/interface">
  <name>Test</name>
  <summary>Placeholder</summary>
  <group main="bin/hello-test">
    %s
    <requires interface="%s">
      <rewrite from="MAGIC_EMBEDDED_STRING_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"/>
    </requires>
  </group>
</interface>
""" % (self._make_archive(main_tree), library_feed))
		selns = selections_for_feed(main_feed)
		cmd_path = run.get_executable_for_selections(selns)
		proc = subprocess.Popen([cmd_path], stdout=subprocess.PIPE)
		stdout, stderr = proc.communicate()
		self.assertEquals(proc.wait(), 0)
		self.assertNotEquals(stdout, original)
		self.assertEquals(len(stdout), len(original))

	def test_exponential_dependency_graph(self):
		# This test will not fail when the code does the wrong
		# thing, it will just take a long time to complete.
		empty_archive = self._make_archive(FileTreeWriter([]))
		library_feed = self.make_temp_file("""\
<?xml version="1.0" ?>
<interface xmlns="http://zero-install.sourceforge.net/2004/injector/interface">
  <name>Test</name>
  <summary>Placeholder</summary>
  <group>%s</group>
</interface>
""" % empty_archive)
		for i in range(50):
			library_feed = self.make_temp_file("""\
<?xml version="1.0" ?>
<interface xmlns="http://zero-install.sourceforge.net/2004/injector/interface">
  <name>Test</name>
  <summary>Placeholder</summary>
  <group>
    %(impl)s
    <environment name="BAR" insert="."/>
    <requires interface="%(lib)s">
      <rewrite from="MAGIC_EMBEDDED_STRING_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"/>
    </requires>
    <requires interface="%(lib)s">
      <rewrite from="MAGIC_EMBEDDED_STRING_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"/>
    </requires>
  </group>
</interface>
""" % {"impl": empty_archive,
       "lib": library_feed})
		main_feed = self.make_temp_file("""
<interface xmlns="http://zero-install.sourceforge.net/2004/injector/interface">
  <name>Test</name>
  <summary>Placeholder</summary>
  <group main="bin/main">
    %s
    <requires interface="%s">
      <environment name="FOO" insert="."/>
    </requires>
  </group>
</interface>
""" % (self._make_archive(FileTreeWriter([("bin/main", "")])), library_feed))
		selns = selections_for_feed(main_feed)
		env = {}
		run.add_env_bindings(env, selns)
		run.get_executable_for_selections(selns)

	def test_circular_substitution_dependency_graph(self):
		main_tree = FileTreeWriter([("share/useful", "circle"),
					    ("bin/hello-test", """\
#!/bin/sh
echo Got:
cat MAGIC_EMBEDDED_STRING_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX/share/useful
""")])
		feed_file = self.make_temp_file("")
		# This is a self-referential cycle, but it treated
		# differently from a self-reference outside a
		# <requires>.
		write_file(feed_file, """
<interface xmlns="http://zero-install.sourceforge.net/2004/injector/interface">
  <name>Test</name>
  <summary>Placeholder</summary>
  <group main="bin/hello-test">
    %s
    <requires interface="%s">
      <rewrite from="MAGIC_EMBEDDED_STRING_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"/>
    </requires>
  </group>
</interface>
""" % (self._make_archive(main_tree), feed_file))
		selns = selections_for_feed(feed_file)
		self.assertRaises(
			run.CyclicDependencyError,
			lambda: self._check_output(selns, "Got:\ncircle\n"))
		# Once cycles are handled, we can enable this check:
		# Check that rewritten copies are cached.
		#self.assertEquals(run.get_executable_for_selections(selns),
		#		  run.get_executable_for_selections(selns))


suite = unittest.makeSuite(TestEndToEnd)
if __name__ == '__main__':
	sys.argv.append('-v')
	unittest.main()
