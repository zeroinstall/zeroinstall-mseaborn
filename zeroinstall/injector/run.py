"""
Executes a set of implementations as a program.
"""

# Copyright (C) 2006, Thomas Leonard
# See the README file for details, or visit http://0install.net.

import errno
import hashlib
import os, sys
from logging import debug, info

from zeroinstall.injector import selections
from zeroinstall.injector.model import SafeException, EnvironmentBinding, ZeroInstallImplementation
from zeroinstall.injector.iface_cache import iface_cache
from zeroinstall.support import basedir

class CyclicDependencyError(Exception):
	pass

def do_env_binding(environ, binding, path):
	"""Update this process's environment by applying the binding.
	@param binding: the binding to apply
	@type binding: L{model.EnvironmentBinding}
	@param path: the selected implementation
	@type path: str"""
	environ[binding.name] = binding.get_value(path,
					environ.get(binding.name, None))
	info("%s=%s", binding.name, environ[binding.name])

def add_env_bindings(environ, sels):
	for selection in sels.selections.values():
		_do_bindings(sels, environ, selection, selection.bindings)
		for dep in selection.dependencies:
			dep_impl = sels.selections[dep.interface]
			if not dep_impl.id.startswith('package:'):
				_do_bindings(sels, environ, dep_impl, dep.bindings)
			else:
				debug("Implementation %s is native; no bindings needed", dep_impl)

def execute(policy, prog_args, dry_run = False, main = None, wrapper = None):
	"""Execute program. On success, doesn't return. On failure, raises an Exception.
	Returns normally only for a successful dry run.
	@param policy: a policy with the selected versions
	@type policy: L{policy.Policy}
	@param prog_args: arguments to pass to the program
	@type prog_args: [str]
	@param dry_run: if True, just print a message about what would have happened
	@type dry_run: bool
	@param main: the name of the binary to run, or None to use the default
	@type main: str
	@param wrapper: a command to use to actually run the binary, or None to run the binary directly
	@type wrapper: str
	@precondition: C{policy.ready and policy.get_uncached_implementations() == []}
	"""
	selns = selections.Selections(policy)
	execute_selections(selns, prog_args, dry_run=dry_run, main=main,
			   wrapper=wrapper)

def _do_bindings(selns, environ, impl, bindings):
	for b in bindings:
		if isinstance(b, EnvironmentBinding):
			do_env_binding(environ, b, _get_implementation_path(selns, impl))

def read_file(filename):
	fh = open(filename, "r")
	try:
		return fh.read()
	finally:
		fh.close()

def write_file(filename, data):
	fh = open(filename, "w")
	try:
		fh.write(data)
	finally:
		fh.close()

def copy_tree(src_path, dest_path, copy_file):
	if os.path.islink(src_path):
		if os.path.islink(dest_path):
			os.unlink(dest_path)
		os.symlink(os.readlink(src_path), dest_path)
	elif os.path.isdir(src_path):
		if not os.path.exists(dest_path):
			os.mkdir(dest_path)
		for leafname in os.listdir(src_path):
			copy_tree(os.path.join(src_path, leafname),
				  os.path.join(dest_path, leafname),
				  copy_file)
	else:
		copy_file(src_path, dest_path)

def rewrite_dir(impl_dir, dep_rewrites, self_rewrites):
	cache_dir = os.path.join(basedir.xdg_cache_home, "0install.net", "rewrites")
	if not os.path.exists(cache_dir):
		os.makedirs(cache_dir)
	digest = hashlib.sha1(str(dep_rewrites + self_rewrites)).hexdigest()
	# We don't need the full path.  Rewrites are local.
	leafname = digest[:8]
	dest_dir = os.path.join(cache_dir, leafname)
	# TODO: fix race condition
	if os.path.exists(dest_dir):
		return dest_dir
	rewrites = dep_rewrites[:]
	for rewrite in self_rewrites:
		dest_dir2 = _make_fixed_length_link(dest_dir, len(rewrite))
		rewrites.append((rewrite, dest_dir2))

	def copy_file(src_path, dest_path):
		data = read_file(src_path)
		# This is not an optimal way to do several
		# substitutions, but when I tried to use the "array"
		# module to do it in-place, it was slower.
		for from_str, to_str in rewrites:
			data = data.replace(from_str, to_str)
		write_file(dest_path, data)
		os.chmod(dest_path, os.stat(src_path).st_mode)

	copy_tree(impl_dir, dest_dir, copy_file)
	return dest_dir

def _make_fixed_length_link(pathname, length):
	link_dir = os.path.join(basedir.xdg_cache_home, "0install.net", "ln")
	if not os.path.exists(link_dir):
		os.makedirs(link_dir)
	digest = hashlib.sha1(pathname).hexdigest()[:8]
	dest_base = os.path.join(link_dir, digest)
	assert len(dest_base) <= length, (dest_base, len(dest_base), length)
	dest_path = dest_base + "X" * (length - len(dest_base))
	try:
		os.symlink(pathname, dest_path)
	except OSError, exn:
		if exn.errno != errno.EEXIST:
			raise
	return dest_path

CYCLIC_REF = object()

def memoize(func):
	# TODO: use weak references?
	cache = {}
	def wrapper(*args):
		try:
			value = cache[args]
		except KeyError:
			cache[args] = CYCLIC_REF
			value = func(*args)
			cache[args] = value
		else:
			if value is CYCLIC_REF:
				raise CyclicDependencyError()
		return value
	return wrapper

@memoize
def _get_implementation_path(selns, impl):
	if impl.id.startswith('/'):
		return impl.id
	rewrites = []
	for dependency in impl.dependencies:
		for rewrite_string in dependency.rewrites:
			dep_impl = selns.selections[dependency.interface]
			to_path = _get_implementation_path(selns, dep_impl)
			to_path = _make_fixed_length_link(to_path, len(rewrite_string))
			rewrites.append((rewrite_string, to_path))
	impl_dir = iface_cache.stores.lookup(impl.id)
	if len(rewrites) == 0 and len(impl.self_rewrites) == 0:
		return impl_dir
	return rewrite_dir(impl_dir, rewrites, impl.self_rewrites)

def execute_selections(selections, prog_args, dry_run = False, main = None, wrapper = None):
	"""Execute program. On success, doesn't return. On failure, raises an Exception.
	Returns normally only for a successful dry run.
	@param selections: the selected versions
	@type selections: L{selections.Selections}
	@param prog_args: arguments to pass to the program
	@type prog_args: [str]
	@param dry_run: if True, just print a message about what would have happened
	@type dry_run: bool
	@param main: the name of the binary to run, or None to use the default
	@type main: str
	@param wrapper: a command to use to actually run the binary, or None to run the binary directly
	@type wrapper: str
	@since: 0.27
	@precondition: All implementations are in the cache.
	"""
	add_env_bindings(os.environ, selections)
	_execute(selections, prog_args, dry_run, main, wrapper)

def test_selections(selections, prog_args, dry_run, main, wrapper = None):
	"""Run the program in a child process, collecting stdout and stderr.
	@return: the output produced by the process
	@since: 0.27
	"""
	args = []
	import tempfile
	output = tempfile.TemporaryFile(prefix = '0launch-test')
	try:
		child = os.fork()
		if child == 0:
			# We are the child
			try:
				try:
					os.dup2(output.fileno(), 1)
					os.dup2(output.fileno(), 2)
					execute_selections(selections, prog_args, dry_run, main)
				except:
					import traceback
					traceback.print_exc()
			finally:
				sys.stdout.flush()
				sys.stderr.flush()
				os._exit(1)

		info("Waiting for test process to finish...")

		pid, status = os.waitpid(child, 0)
		assert pid == child

		output.seek(0)
		results = output.read()
		if status != 0:
			results += "Error from child process: exit code = %d" % status
	finally:
		output.close()
	
	return results

def get_executable_for_selections(selns, main=None):
	root_impl = selns.selections[selns.interface]
	assert root_impl is not None

	if root_impl.id.startswith('package:'):
		main = main or root_impl.main
		prog_path = main
	else:
		if main is None:
			main = root_impl.main
		elif main.startswith('/'):
			main = main[1:]
		elif root_impl.main:
			main = os.path.join(os.path.dirname(root_impl.main), main)
		if main:
			prog_path = os.path.join(_get_implementation_path(selns, root_impl), main)

	if main is None:
		raise SafeException("Implementation '%s' cannot be executed directly; it is just a library "
				    "to be used by other programs (or missing 'main' attribute)" %
				    root_impl)

	if not os.path.exists(prog_path):
		raise SafeException("File '%s' does not exist.\n"
				"(implementation '%s' + program '%s')" %
				(prog_path, root_impl.id, main))
	return prog_path

def _execute(selns, prog_args, dry_run, main, wrapper):
	prog_path = get_executable_for_selections(selns, main)
	if wrapper:
		prog_args = ['-c', wrapper + ' "$@"', '-', prog_path] + list(prog_args)
		prog_path = '/bin/sh'

	if dry_run:
		print "Would execute:", prog_path, ' '.join(prog_args)
	else:
		info("Executing: %s", prog_path)
		sys.stdout.flush()
		sys.stderr.flush()
		try:
			os.execl(prog_path, prog_path, *prog_args)
		except OSError, ex:
			raise SafeException("Failed to run '%s': %s" % (prog_path, str(ex)))
